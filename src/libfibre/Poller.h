/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Poller_h_
#define _Poller_h_ 1

#include "runtime/Debug.h"
#include "runtime/Stats.h"

class BaseProcessor;

#include <unistd.h>      // close
#if __FreeBSD__
#include <sys/event.h>
#else // __linux__ below
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#endif

class EventScope;
class Fibre;
class Fred;

class BasePoller {
public:
#if __FreeBSD__
  static const size_t Input  = 0x1;
  static const size_t Output = 0x2;
#else // __linux__ below
  static const size_t Input  = EPOLLIN | EPOLLPRI | EPOLLRDHUP;
  static const size_t Output = EPOLLOUT;
#endif

protected:
#if __FreeBSD__
  typedef struct kevent EventType;
  enum PollFlag : size_t { Level = 0, Edge = EV_CLEAR, Oneshot = EV_ONESHOT };
#else // __linux__ below
  typedef epoll_event   EventType;
  enum PollFlag : size_t { Level = 0, Edge = EPOLLET, Oneshot = EPOLLONESHOT };
#endif

  static const int maxPoll = 1024;
  EventType     events[maxPoll];
  int           pollFD;

  EventScope&   eventScope;
  volatile bool pollTerminate;

  PollerStats* stats;

  template<bool Blocking>
  inline int doPoll();

  template<bool Enqueue = true>
  inline Fred* notifyOne(EventType& ev);

  inline void notifyAll(int evcnt);

public:
  BasePoller(EventScope& es, cptr_t parent, const char* n = "BasePoller") : eventScope(es), pollTerminate(false) {
    stats = new PollerStats(this, parent, n);
#if __FreeBSD__
    pollFD = SYSCALLIO(kqueue());
#else // __linux__ below
    pollFD = SYSCALLIO(epoll_create1(EPOLL_CLOEXEC));
#endif
    DBG::outl(DBG::Level::Polling, "Poller ", FmtHex(this), " create ", pollFD);
  }

  ~BasePoller() {
    SYSCALL(close(pollFD));
  }

#if TESTING_ONESHOT_REGISTRATION
  template<PollFlag pflag = Oneshot>
#else
  template<PollFlag pflag = Edge>
#endif
  void setupFD(int fd, size_t status, bool change = false) {
    DBG::outl(DBG::Level::Polling, "Poller ", FmtHex(this), " register ", fd, " on ", pollFD, " for ", status);
    stats->regs.count();
#if __FreeBSD__
    struct kevent ev[2];
    int idx = 0;
    if (status & Input) {
      EV_SET(&ev[idx], fd, EVFILT_READ, EV_ADD | pflag, 0, 0, 0);
      idx += 1;
    }
    if (status & Output) {
      EV_SET(&ev[idx], fd, EVFILT_WRITE, EV_ADD | pflag, 0, 0, 0);
      idx += 1;
    }
    SYSCALL(kevent(pollFD, ev, idx, nullptr, 0, nullptr));
#else // __linux__ below
    epoll_event ev;
    ev.events = pflag | status; // man 2 epoll_ctl: EPOLLERR, EPOLLHUP not needed
    ev.data.fd = fd;
    if (change) {
      SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_MOD, fd, &ev));
    } else {
      SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_ADD, fd, &ev));
    }
#endif
  }

  void resetFD(int fd) {
    DBG::outl(DBG::Level::Polling, "Poller ", FmtHex(this), " deregister ", fd, " on ", pollFD);
#if __FreeBSD__
    struct kevent ev[2];
    EV_SET(&ev[0], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
    EV_SET(&ev[1], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
    SYSCALL(kevent(pollFD, ev, 2, nullptr, 0, nullptr));
#else // __linux__ below
    SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_DEL, fd, nullptr));
#endif
  }
};

class BaseThreadPoller : public BasePoller {
  pthread_t pollThread;

protected:
  BaseThreadPoller(EventScope& es, cptr_t parent, const char* n) : BasePoller(es, parent, n) {}
  void start(void *(*loopSetup)(void*)) {
    SYSCALL(pthread_create(&pollThread, nullptr, loopSetup, this));
  }

  template<typename T>
  static inline void pollLoop(T& This);

public:
  pthread_t getSysThreadId() { return pollThread; }

  void terminate(_friend<EventScope>) {
    pollTerminate = true;
#if __FreeBSD__
    struct kevent waker;
    EV_SET(&waker, 0, EVFILT_USER, EV_ADD | EV_CLEAR, 0, 0, 0);
    SYSCALL(kevent(pollFD, &waker, 1, nullptr, 0, nullptr));
    EV_SET(&waker, 0, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER, 0, 0);
    SYSCALL(kevent(pollFD, &waker, 1, nullptr, 0, nullptr));
    DBG::outl(DBG::Level::Polling, "Poller ", FmtHex(this), " woke ", pollFD);
    SYSCALL(pthread_join(pollThread, nullptr));
#else // __linux__ below
    int waker = SYSCALLIO(eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK)); // binary semaphore semantics w/o EFD_SEMAPHORE
    setupFD(waker, Input);
    uint64_t val = 1;
    val = SYSCALL_EQ(write(waker, &val, sizeof(val)), sizeof(val));
    DBG::outl(DBG::Level::Polling, "Poller ", FmtHex(this), " woke ", pollFD, " via ", waker);
    SYSCALL(pthread_join(pollThread, nullptr));
    SYSCALL(close(waker));
#endif
  }
};

class PollerFibre : public BasePoller {
  Fibre* pollFibre;
  inline void pollLoop();
  static void pollLoopSetup(PollerFibre*);
public:
  PollerFibre(EventScope&, BaseProcessor&, cptr_t parent, bool bg = true);
  ~PollerFibre();
  void start();
};

class PollerThread : public BaseThreadPoller {
  static void* pollLoopSetup(void*);
public:
  PollerThread(EventScope& es, BaseProcessor&, cptr_t parent) : BaseThreadPoller(es, parent, "PollerThread") {}
  void prePoll(_friend<BaseThreadPoller>) {}
  void start() { BaseThreadPoller::start(pollLoopSetup); }
};

class MasterPoller : public BaseThreadPoller {
  int timerFD;
  static void* pollLoopSetup(void*);

public:
#if __FreeBSD__
  static const int extraTimerFD = 1;
#else
  static const int extraTimerFD = 0;
#endif

  MasterPoller(EventScope& es, unsigned long fdCount, _friend<EventScope>) : BaseThreadPoller(es, &es, "MasterPoller") {
    BaseThreadPoller::start(pollLoopSetup);
#if __FreeBSD__
    timerFD = fdCount - 1;
#else
    timerFD = SYSCALLIO(timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK | TFD_CLOEXEC));
    setupFD<Edge>(timerFD, Input);
#endif
  }

#if __linux__
  ~MasterPoller() { SYSCALL(close(timerFD)); }
#endif

  inline void prePoll(_friend<BaseThreadPoller>);

  void setTimer(const Time& reltimeout) {
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, timerFD, EVFILT_TIMER, EV_ADD | EV_ONESHOT, NOTE_USECONDS, reltimeout.toUS(), 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
#else
    itimerspec tval = { {0,0}, reltimeout };
    SYSCALL(timerfd_settime(timerFD, 0, &tval, nullptr));
#endif
  }

  void setupPollFD(int fd, bool change) { // use hierarchical pollling via ONESHOT
    setupFD<Oneshot>(fd, Input, change);
  }
};

#endif /* _Poller_h_ */
